import React, { useEffect, useState } from 'react';

function AutomobilesList() {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8100/api/automobiles')
      .then(response => response.json())
      .then(data => setAutomobiles(data.autos))
      .catch(error => console.error('Error:', error));
  }, []);

  return (
    <>
      <h1 className="mb-3 mt-3">Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => (
            <tr key={automobile.vin}>
              <td>{automobile.vin}</td>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.model.name}</td>
              <td>{automobile.model.manufacturer.name}</td>
              <td>{automobile.sold ? 'Yes' : 'No'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default AutomobilesList;
