import React, { useEffect, useState } from "react";


function ModelForm() {
    const [name, setName] = useState("");
    const [manufacturers, setManufacturers] = useState([]);
    const [manufacturer, setManufacturer] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [state, setState] = useState(false);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
        }
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = pictureUrl;
        // include id for manufacturer
        data.manufacturer_id = manufacturer;

        const ModelUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json",
        },
        };
        const response = await fetch(ModelUrl, fetchConfig);
        if (response.ok) {
        const newModel = await response.json();
        setName("");
        setPictureUrl("");
        setManufacturer("");
        setState(true);
        }
        else {
            setState(false);
        }
    };

    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    };

    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    };



    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <form id="create-model-form" onSubmit={handleSubmit}>
                <h1>Create a new model</h1>
                <div className="form-floating mb-3">
                <input
                    onChange={handleNameChange}
                    placeholder="Name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                    value={name}
                />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input
                    onChange={handlePictureUrlChange}
                    placeholder="PictureUrl"
                    required
                    type="text"
                    name="PictureUrl"
                    id="PictureUrl"
                    className="form-control"
                    value={pictureUrl}
                />
                <label htmlFor="PictureUrl">Picture url</label>
                </div>
                <div className="mb-3">
                <select
                    onChange={handleManufacturerChange}
                    required
                    // calling wrong id and name
                    id="manufacturer"
                    name="manufacturer"
                    className="form-select"
                    value={manufacturer}
                >
                    <option value="">Choose a manufacturer</option>
                    {manufacturers.map((manufacturer) => (
                    // calling href instead of id
                    <option value={manufacturer.id} key={manufacturer.id}>
                        {manufacturer.name}
                    </option>
                    ))}
                </select>
                </div>
                <div>
                <button className="btn btn-primary">Create</button>
                </div>
            </form>
            </div>
            <p></p>
            {state?
            <div className="alert alert-success mb-0" id="success">
            Successfully created model!
            </div>:<div></div>
            }
        </div>
        </div>
    );
    }

export default ModelForm;
