import React,  { useEffect, useState } from 'react';

function ModelList() {
    const [models, setModels] = useState([]);
    const fetchData = async() => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(
        () => {
            fetchData();
        }, [])

    return (
        <div className="container">
        <h1 className="mb-3 mt-3">Models</h1>
          <table className='table table-striped'>
            <thead>
              <tr className="align-middle text-center">
                <th scope='col'>Name</th>
                <th scope='col'>Manufacturer</th>
                <th scope='col'>Picture</th>
              </tr>
            </thead>
            <tbody>
                {models.map(model => {
                    return (
                        <tr className="align-middle text-center" key={model.id}>
                            <td>{model.name}</td>
                            <td>{model.manufacturer.name}</td>
                            <td><img src={model.picture_url} style = {{width: "200px"}}/></td>
                        </tr>
                    )
                })}
            </tbody>
          </table>
        </div>
    )


}
export default ModelList;
