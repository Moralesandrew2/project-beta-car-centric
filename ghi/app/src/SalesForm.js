import React, { useState, useEffect } from 'react';

function CreateSale() {
  const [automobile, setAutomobile] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [salesperson, setSalesPerson] = useState('');
  const [salespeople, setSalesPeople] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');

  const fetchAutomobiles = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      const availableAutos = data.autos.filter(auto => !auto.sold);
      setAutomobiles(availableAutos);
    }
  };

  const fetchSalesPeople = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.sales_employee);
    }
  };

  const fetchCustomers = async () => {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  };

  useEffect(() => {
    fetchAutomobiles();
    fetchCustomers();
    fetchSalesPeople();
  }, []);

  const handleChange = (event, callback) => {
    const { target } = event;
    const { value } = target;
    callback(value);
  };

  const updateAutomobileSoldStatus = async (auto) => {
    const url = `http://localhost:8100/api/automobiles/${auto}/`;
    const fetchConfig = {
      method: 'PUT',
      body: JSON.stringify({ sold: true }),
      headers: { 'Content-Type': 'application/json' },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      console.log('Updated sold to true.');
    } else {
      console.error('Failed to update sold to true.');
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      price: price,
      customer: customer,
      salesperson: salesperson,
      automobile: automobile,
    };

    const json = JSON.stringify(data);
    console.log(json)
    const url = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: 'POST',
      body: json,
      headers: { 'Content-Type': 'application/json' },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      console.log(newSale);

      await updateAutomobileSoldStatus(automobile);

      setCustomer('');
      setPrice('');
      setAutomobile('');
      setSalesPerson('');
    }
  };

  return (
    <div className="justify-content-center align-items-center">
      <div className='shadow p-4 mt-4'>
        <h1>Record a new sale</h1>
        <form onSubmit={handleSubmit}>
          <div>
            <label>Automobile VIN</label>
          </div>
          <div className="form-floating mb-3">
            <select className="form-select" onChange={(event) => { handleChange(event, setAutomobile) }} value={automobile} required name="automobile" id="automobile">
              <option>Choose an automobile VIN...</option>
              {automobiles.map((automobile) => {
                return (
                  <option key={automobile.id} value={automobile.vin}>
                    {automobile.vin}
                  </option>
                )
              })}
            </select>
          </div>
          <div>
            <label>Salesperson</label>
          </div>
          <div className="form-floating mb-3">
            <select className="form-select" onChange={(event) => { handleChange(event, setSalesPerson) }} value={salesperson} required name="salesperson" id="salesperson">
              <option>Choose a salesperson...</option>
              {salespeople.map((salesperson) => {
                return (
                  <option key={salesperson.employee_id} value={salesperson.employee_id}>
                    {salesperson.first_name} {salesperson.last_name}
                  </option>
                )
              })}
            </select>
          </div>
          <div>
            <label>Customer</label>
          </div>
          <div className="form-floating mb-3">
            <select className="form-select" onChange={(event) => { handleChange(event, setCustomer) }} value={customer} required name="customer" id="customer">
              <option>Choose a customer...</option>
              {customers.map((customer) => {
                return (
                  <option key={customer.id} value={customer.id}>
                    {customer.first_name} {customer.last_name}
                  </option>
                )
              })}
            </select>
          </div>
          <div>
            <label>Price</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={(event) => { handleChange(event, setPrice) }} value={price} required name="price" id="price" />
          </div>
          <div className="form-floating mb-3">
            <button className="btn btn-primary">Create</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export default CreateSale;
