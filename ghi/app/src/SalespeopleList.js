
import React, { useEffect, useState } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
          const data = await response.json();
          setSalespeople(data.sales_employee);
        } else {
          console.error('Error:', response.statusText);
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchData();
  }, []);

  const deleteButtonClick = async (salesperson) => {
    const confirm = window.confirm(`Are you sure you want to delete ${salesperson.first_name} ${salesperson.last_name}?`);
    if (confirm) {
      const salespersonId = salesperson.id;
      const salespersonUrl = `http://localhost:8090/api/salespeople/${salespersonId}`;
      const fetchConfig = { method: "delete" };
      try {
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
          const updatedSalespeople = salespeople.filter(item => item.id !== salespersonId);
          setSalespeople(updatedSalespeople);
        } else {
          console.error('Error:', response.statusText);
        }
      } catch (error) {
        console.error('Error:', error);
      }
    }
  };

  return (
    <>
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map(salesperson => (
            <tr key={salesperson.id}>
              <td>{salesperson.employee_id}</td>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
              <td>
                <button onClick={() => deleteButtonClick(salesperson)} type="button" className="btn btn-outline-danger">Delete Salesperson</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default SalespeopleList;
