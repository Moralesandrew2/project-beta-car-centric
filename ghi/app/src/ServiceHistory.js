import React, { useEffect, useState} from 'react';


function ServiceHistory(){

    const [appointment, setAppointment] = useState('');
    const [appointments, setAppointments] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');

    const fetchData = async() => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
            for (let appointment of data.appointments){
                const date = new Date(appointment.date_time).toLocaleDateString();
                appointment["date"] = date;
                const time =  new Date(appointment.date_time).toLocaleTimeString();
                appointment["time"] = time;

            }
        }
        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
            const automobileResponse = await fetch(automobileUrl);

            if (automobileResponse.ok) {
                const automobileData = await automobileResponse.json();
                setAutomobiles(automobileData.AutomobileVOs);
            }

    }

    useEffect(
        () => {
            fetchData();
        }, []
    )

    const handleSearch = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            for (let appointment of data.appointments){
                const date = new Date(appointment.date_time).toLocaleDateString();
                appointment["date"] = date;
                const time =  new Date(appointment.date_time).toLocaleTimeString();
                appointment["time"] = time;
            }
            setAppointments(updatedAppointments => updatedAppointments.filter(appointment => appointment.vin === searchTerm));
        }
        const automobileUrl = 'http://localhost:8080/api/automobileVOs/'
            const automobileResponse = await fetch(automobileUrl);

            if (automobileResponse.ok) {
                const automobileData = await automobileResponse.json();
                setAutomobiles(automobileData.AutomobileVOs);
            }


    }




    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value)
    }


        let vins = [];
        {automobiles.map(automobile => {

            vins.push(automobile.vin)
        })}

    return (
        <div className="container">
            <h1 className="mb-3 mt-3">Service History</h1>
                <form className='col' onSubmit={handleSearch}>
                    <input className="form-control" id="searchbar" placeholder="Search by VIN..." value={searchTerm} onChange={handleSearchChange}></input>
                    <button className="btn btn-primary btn-sm">Search</button>
                </form>
                <button className="btn btn-success btn-sm" onClick={fetchData}>Refresh History</button>
            <table className='table table-striped'>
                <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
                </thead>

                <tbody>
                    {appointments.map((appointment, index) => {
                        {if (vins.includes(appointment.vin)){
                            var vip = "Yes"
                        }
                        else {var vip = "No"}}
                        return (
                            <tr key={index}>
                                <td>{appointment.vin}</td>
                                <td>{vip}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

            )



    }


export default ServiceHistory;
