from django.contrib import admin
from .models import Sales, AutomobileVO
# Register your models here.

admin.site.register(Sales)

admin.site.register(AutomobileVO)
