import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles/")
    content = json.loads(response.content)

    automobiles = content["autos"]

    for automobile in automobiles:
        vin = automobile["vin"]
        sold = automobile.get("sold")
        if not sold:
            AutomobileVO.objects.update_or_create(
                vin=vin,
                defaults={"sold": sold},
            )
            print(f"Polled automobile with VIN: {vin}")

def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)

        # Print the saved instances for troubleshooting
        automobiles = AutomobileVO.objects.all()
        print("Saved Automobiles:")
        for automobile in automobiles:
            print(f"VIN: {automobile.vin}, Sold: {automobile.sold}")

        if not repeat:
            break
        time.sleep(60)

if __name__ == "__main__":
    poll()
