from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField()


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default="scheduled")
    vin = models.CharField(max_length=17, null=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician, related_name="technician",
        on_delete=models.CASCADE)

    def cancel(self):
        self.status = "canceled"
        self.save()

    def finish(self):
        self.status = "finished"
        self.save()
